/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Class to handle the logic needed on caseInterTrigger
*   
*
*   Called by: caseInterTrigger
*   Test Classes: caseInterTriggerTest
*
* NAME: CaseInternTriggerHandler
* AUTHOR: João Vitor Ramos                                 DATE:28/01/2021
*******************************************************************************/
public class CaseInternTriggerHandler {
    public static void fecharMarco(list<Case> casos, map<Id, Case> oldCasos){
        //lists to handle the logic
        list<Id> caseStatusChanged = new list<Id>();
        list<CaseMilestone> cmList = new list<CaseMilestone>();
        list<CaseMilestone> cmUpdList = new list<CaseMilestone>();

        //add to caseStatusChanged the cases with the Status is changing
        for(Case cs : casos){
            if(cs.Status != oldCasos.get(cs.id).Status){
                caseStatusChanged.add(cs.Id);
            }
        }

        //Retrieving the open CaseMilestones of cases that is changing the status
        if(!caseStatusChanged.isEmpty()){
            cmList = [SELECT Id, CaseId, CompletionDate, isCompleted FROM CaseMilestone 
                      WHERE isCompleted = false AND CaseId IN :caseStatusChanged];
        }

        //Completing the CaseMilestone before changing the status on Cases
        if(!cmList.isEmpty()){
            for(CaseMilestone ms : cmList){
                ms.CompletionDate = DateTime.now();
                cmUpdList.add(ms);
            }
        update cmUpdList;
        }  
    }

    public static void adicionarContatoConta(list<Case> casos){
        //lists to handle the logic
        list<Contact> contactList = new list<Contact>();
        list<Case> caseComWebEmailList = new list<Case>();
        list<String> emailList = new list<String>();
        map<string,string> contactEmailIdMap = new map<string,string>();
        map<string,string> accountEmailIdMap = new map<string,string>();

        //passing through all contacts and validating if it has the SuppliedEmail filled
        //and creating 2 lists to retrieve contacts related this emails
        for(Case cs : casos){
            if(cs.SuppliedEmail != null){
                Boolean result = cs.SuppliedEmail.contains('@');
                if(cs.SuppliedEmail != '' && cs.SuppliedEmail != ' ' && result == true){
                    emailList.add(cs.SuppliedEmail);
                    caseComWebEmailList.add(cs);
                }
            }
        }

        //retrieving the contacts and its accounts
        if(emailList != null){
            contactList = [select id, email, account.id from contact where email in :emailList];
        }

        //creating the map with email and Id of contacts and accounts
        if(contactList != null){
            for(Case cs : caseComWebEmailList){
                for(Contact ctt : contactList){
                    if(cs.SuppliedEmail == ctt.email){
                        contactEmailIdMap.put(cs.SuppliedEmail, ctt.Id);
                        accountEmailIdMap.put(cs.SuppliedEmail, ctt.Account.Id);
                    }
                }
            }
        }

        //matching the contactId and AccountId with some contact using the email
        for(Case cs : caseComWebEmailList){
            if(cs.ContactId == null && cs.SuppliedEmail != null){
                cs.ContactId = contactEmailIdMap.get(cs.SuppliedEmail);
                cs.AccountId = accountEmailIdMap.get(cs.SuppliedEmail);
            }
        }

        //filling the webName when the case came from forms
        //and filling the Nomeforms when the case came from email
        for(Case cs : casos){
            if(cs.SuppliedName != null && cs.Nome__c == null){
                cs.Nome__c = cs.SuppliedName;
            }else if(cs.SuppliedName == null && cs.Nome__c != null){
                cs.SuppliedName = cs.Nome__c;
            }
        }
    }
}