/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Test class to test the case trigger and its handle class
*   
*
*   Calling : CaseInterTrigger, CaseInternTriggerHandler
*
* NAME: CaseInternTriggerTest
* AUTHOR: João Vitor Ramos                                 DATE:28/01/2021
*******************************************************************************/
@isTest
public class CaseInternTriggerTest {

    @TestSetup
    static void makeData(){
        list<Account> accList = new list<Account>();
        list<Contact> cttList = new list<Contact>();

        //creating accounts
        for(Integer i=0; i<10; i++){
            Account acc = new Account();
            acc.Name = 'Acc Test'+(i+1);
            accList.add(acc);
        }
        insert accList;


        //creating Account's contacts
        Integer i = 1;
        for(Account acc : accList){
            Contact ctt = new Contact();
            ctt.FirstName = 'Ctt Test';
            ctt.LastName = ''+i;
            ctt.AccountId = acc.Id;
            ctt.Email = 'test'+i+'@test.com';
            cttList.add(ctt);
            i++;
        }
        insert cttList;
    }

    @isTest
    private static void fechandoMarco(){

        //creating account
        Account acc = new Account(Name='Conta teste');
        insert acc;

        //retrieving an existing Entitlement process (only way to get it in API 51.0, we can't create programmatically)
        SlaProcess EntitleProcess = [select id from SlaProcess where Name = 'Melhoria' Limit 1];

        //creating an Entitlement embedded to account
        Entitlement entl = new entitlement(name='Test Entilement',accountid=acc.id, StartDate=Date.valueof(System.now().addDays(-2)), 
                                            EndDate=Date.valueof(System.now().addYears(2)), SlaProcessId=EntitleProcess.Id);
        insert entl;

        //creating an case embedded to account with an entitlement
        Case cs = new Case(Subject='Test case', status='Deploy', type='Melhoria', Description='Testing', AccountId=acc.Id, entitlementId=entl.Id);
        insert cs;

        //validating if the case has and Case Entitlement active
        System.assertEquals(1, [select count() from CaseMilestone where CaseId = :cs.Id and isCompleted = false]);
        
        //changing the case status to fire the trigger and close de Case Entitlement
        cs.Status = 'Homologação';
        update cs;

        //validating if the case has and Case Entitlement completed
        System.assertEquals(1, [select count() from CaseMilestone where CaseId = :cs.Id and isCompleted = true]);       
    }

    @isTest
    private static void novoCasoPositivo(){
        list<Case> csList = new list<Case>();

        for(Integer i=0; i<10;i++){
            Case cs = new Case();
            cs.subject = 'Test Case'+(i+1);
            cs.Description = 'Test Description';
            cs.Status = getStatus();
            cs.Priority = getPriority();
            cs.Origin = getOrigin();
            cs.Nuvem__c = getNuvem();
            cs.Type = getType();
            cs.Demanda_Dev__c = true;
            cs.SuppliedEmail = 'test'+(i+1)+'@test.com';
            csList.add(cs);
            if(Math.mod(i,2) == 0){
                cs.SuppliedName = 'Name test';
            } else {
                cs.Nome__c = 'Name test';
            }
        }

        Test.startTest();
            insert csList;
        Test.stopTest();

        System.assertEquals(10, [select count() from case where AccountId != null]);
        System.assertEquals(10, [select count() from case where ContactId != null]);
    }

    @isTest
    private static void novoCasoNegativo(){
        list<Case> csList = new list<Case>();

        for(Integer i=0; i<10;i++){
            Case cs = new Case();
            cs.subject = 'Test Case'+(i+1);
            cs.Description = 'Test Description';
            cs.Status = getStatus();
            cs.Priority = getPriority();
            cs.Origin = getOrigin();
            cs.Nuvem__c = getNuvem();
            cs.Type = getType();
            cs.Demanda_Dev__c = true;
            cs.SuppliedEmail = 'testeNegativo'+(i+1)+'@test.com';
            csList.add(cs);
        }

        Test.startTest();
            insert csList;
        Test.stopTest();
        for(Case acc: csList){
            System.debug('case email: '+ acc.SuppliedEmail);
        }

        System.assertEquals(0, [select count() from case where AccountId != null]);
        System.assertEquals(0, [select count() from case where ContactId != null]);
    }

    //methods to take the first value of case picklists
    public static String getStatus(){
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Case.Status.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
        return pickListValuesList[0];
    }

    public static String getPriority(){
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Case.Priority.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
        return pickListValuesList[0];
    }
    public static String getOrigin(){
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Case.Origin.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
        return pickListValuesList[0];
    }
    public static String getNuvem(){
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Case.Nuvem__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
        return pickListValuesList[0];
    }
    public static String getType(){
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Case.Type.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
        return pickListValuesList[0];
    }
}